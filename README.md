---
Author: Marco Ferreira
Email: m@roc.cc
---

# Frontmen Assignment

### Projects
* Chuck Jokes
  - Code is split into smaller parts, which in my opinion makes bigger projects easier to maintain
* Login Popup
  - 'Ducks' structure, where component files hold everything that belongs to them

### Stack
* TypeScript
* React
* Styled Components
  - Automatic style scoping
  - No classnames to think about
  - Cleaner components, as there's no classname visual noise