import { setJokeStorage } from '../utils/joke-storage'
import { TJoke } from '../components/types'

export const initialState: {
  jokes: TJoke[], 
  favJokes: TJoke[], 
  maxJokes: number, 
  full: boolean
  favJokesFull: boolean
} = {
  jokes: [], 
  favJokes: [], 
  maxJokes: 0,
  full: false,
  favJokesFull: false,
}

type ReducerAction = {
  type: string, 
  jokes?: TJoke[],
  maxJokes?: number
}

export function reducer(
  state: typeof initialState, 
  action: ReducerAction
){
  const { jokes = [], maxJokes } = action
  
  switch (action.type) {
    case 'addJokes':
      const newJokes = [
        ...state.jokes,
        ...jokes,
      ]
      return {
        ...state,
        jokes: newJokes,
        full: newJokes.length >= state.maxJokes,
      }

    case 'addFavJokes': {
      const newFavJokes = [
        ...state.favJokes,
        ...jokes,
      ]
      setJokeStorage(newFavJokes)

      return {
        ...state,
        favJokes: newFavJokes,
        favJokesFull: newFavJokes.length >= state.maxJokes
      }
    }

    case 'delFavJokes': {
      const [joke] = jokes
      const newFavJokes = state.favJokes.filter(
        favJoke => favJoke.id !== joke.id,
      )
      return {
        ...state,
        favJokes: newFavJokes,
        favJokesFull: newFavJokes.length >= state.maxJokes
      }
    }

    case 'resetJokes':
      return {
        ...state,
        maxJokes: maxJokes!,
        jokes: jokes,
        full: false
      }

    case 'resetFavJokes':
      return {
        ...state,
        maxJokes: maxJokes!,
        favJokes: jokes.length ? jokes : state.favJokes,
        favJokesFull: false
      }

    default:
      throw new Error();
  }
}

