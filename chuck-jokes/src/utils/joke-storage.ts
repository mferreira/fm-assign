import { TJoke } from '../components/types'

export const setJokeStorage = (jokes: TJoke[]) => {
  localStorage.setItem('jokeStorage', JSON.stringify(jokes))
}

export const getJokeStorage = (): TJoke[] => {
  const jokes = localStorage.getItem('jokeStorage')
  return jokes ? JSON.parse(jokes) : []
}