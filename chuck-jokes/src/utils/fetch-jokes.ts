import fetch from 'unfetch'

export const fetchJokes = async ({ fetchAmount }: {fetchAmount: number}) => {
  const res = await fetch(`https://api.icndb.com/jokes/random/${fetchAmount}`)
  return await res.json()
}
