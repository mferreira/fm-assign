import React from 'react'
import { render } from 'react-dom'
import { Jokes } from './components/Jokes'

function App() {
  return (
    <div>
      <Jokes amount={10}/>
    </div>
  )
}

const rootElement = document.getElementById('root')
render(<App />, rootElement)
