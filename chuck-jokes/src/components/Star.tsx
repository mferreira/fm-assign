import React from "react"

const EMPTY_STAR = '\u2606'
const FULL_STAR = '\u2605'

export const Star = ({ body, ...rest }: {body: 'full' | 'empty'}) => 
  <span {...rest}>{body === 'empty' ? EMPTY_STAR : FULL_STAR}</span>
