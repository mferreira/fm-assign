import React from 'react'
import styled from 'styled-components'
import { JokeProps } from './types'
import { Star } from './Star'

const JokeBase = styled.div`
  padding: 8px;
  background-color: white;
`

export const Joke = ({ joke, isFavourite, onClick }: JokeProps) => (
  <JokeBase onClick={() => onClick(joke)}>
    {<Star body={isFavourite ? 'full' : 'empty'} />} {joke.joke}
  </JokeBase>
)
