import React, { useCallback, useState, useReducer, useEffect } from 'react'
import styled from 'styled-components'
import { fetchJokes } from '../utils/fetch-jokes'
import { getJokeStorage } from '../utils/joke-storage'
import { reducer, initialState } from '../utils/joke-state'
import { TJoke, JokesProps, FetchNewJokesArgs} from './types'
import { Joke } from './Joke'

const JokeList = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 600px;
`

export const Jokes = ({ amount }: JokesProps) => {
  const [state, dispatch] = useReducer(reducer, initialState)
  const [timerId, setTimerId] = useState<number | undefined>(undefined)

  useEffect(() => {
    const favJokes = getJokeStorage()
    dispatch({type: 'resetFavJokes', jokes: favJokes, maxJokes: amount})
  }, [amount])

  // fetches new jokes and stores in state
  const fetchNewJokes = useCallback(async ({ fetchAmount, reset, isFavourite = false }: FetchNewJokesArgs) => {    
    try {
      const freshJokes: { value: TJoke[] } = await fetchJokes({ fetchAmount })

      if (reset) {
        dispatch({
          type: `reset${!isFavourite ? 'Jokes' : 'FavJokes'}`, 
          jokes: freshJokes.value, 
          maxJokes: amount
        })
      }
      else {
        dispatch({
          type: `add${!isFavourite ? 'Jokes' : 'FavJokes'}`, 
          jokes: freshJokes.value
        })
      }
    } catch (err) {
      console.error('ERROR', JSON.stringify(err))
    }
  }, [amount])
  
  // fetches 'amount' of jokes
  const fetchAllJokes = useCallback(() => {
    fetchNewJokes({ fetchAmount: amount, reset: true })
  }, [amount, fetchNewJokes])

  // will add a random joke to favourites every 5sec
  const startTimer = useCallback(() => {
    // if there is an active timer, clear it
    if (timerId) {
      clearInterval(timerId)
    }

    // don't start the timer if we already have 10 favourites
    if (state.favJokesFull) {
      return
    }
    
    // first joke fetch
    fetchNewJokes({ fetchAmount: 1, isFavourite: true})

    // create new timer and store the id
    setTimerId(
      setInterval(() => {
        fetchNewJokes({ fetchAmount: 1, isFavourite: true })
      }, 500)
    )
  }, [timerId, fetchNewJokes, state])

  const handleJokeClick = (joke: TJoke) => {
    if (state.favJokes.includes(joke)) {
      dispatch({type: 'delFavJokes', jokes: [joke]})
    } else if (!state.favJokesFull) {
      dispatch({type: 'addFavJokes', jokes: [joke]})
    }
  }

  // stop timer if favourite list is full
  if (state.favJokesFull && timerId) {
    clearInterval(timerId)
    setTimerId(undefined)
    // dispatch({type: 'resetFavJokes', maxJokes: amount})
  }

  return (
    <>
      <button onClick={fetchAllJokes}>
        Get Jokes
      </button>
      <button onClick={startTimer}>
        Start Timer
      </button>

      <h4>Jokes</h4>
      {state.jokes.length ? (
        <>
          <p>* Click to add to Favourites</p>
          <JokeList>
            {state.jokes.map((joke, index) => {
              if (state.favJokes.includes(joke)) {
                return null
              }
              return <Joke
                joke={joke}
                isFavourite={false}
                onClick={handleJokeClick}
                key={joke.id+index}
              />
            })}
          </JokeList>
        </>
      ) : (
        <p>No jokes loaded yet</p>
      )}

      <h4>Favourites</h4>
      {state.favJokes.length ? (
        <>
          <p>* Click to remove from Favourites</p>
          <JokeList>
            {state.favJokes.map((joke, index) => {
              return <Joke
                joke={joke}
                isFavourite={true}
                onClick={handleJokeClick}
                key={joke.id+index}
              />
            })}
          </JokeList>
        </>
      ) : (
        <p>No favourite jokes :(</p>
      )}
    </>
  )
}
