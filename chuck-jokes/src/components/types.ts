export type TJoke = {
  id: number
  joke: string
  categories: string[]
}

export interface JokeProps {
  joke: TJoke
  isFavourite: boolean
  onClick: (joke: TJoke) => void
}

export interface JokesProps {
  amount: number
}

export interface FetchNewJokesArgs { 
  fetchAmount: number 
  reset?: boolean 
  isFavourite?: boolean
}

