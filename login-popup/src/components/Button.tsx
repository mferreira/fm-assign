// import React from 'react'
import styled from 'styled-components'

interface Props {
  gutter?: boolean
}

export const Button = styled('button')<Props>`
  padding: 6px 20px;
  margin-right: ${p => p.gutter ? '16px' : '0'};
`
