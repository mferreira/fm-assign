import React, { useCallback, useReducer, useEffect } from 'react'
import styled from 'styled-components'
import { Input } from './Input'
import { Button } from './Button'
import { Popup } from './Popup'

type State = {
  username: string
  password: string
  validForm: boolean
}

type Action = {
  type: string
  payload?: any
}

const initialState: State = {
  username: '',
  password: '',
  validForm: false,
}

function validatePassword(password: string) {
  const passLength = password.length

  if (
    !passLength 
    || passLength > 32
    || /i|O|I/g.exec(password)
    || /[A-Z]/g.exec(password)
  ) return false

  let increasingStraight = false
  let nonOverlap = 0

  for (let i = 0; i <= passLength; i++) {
    // check for invcreasing straight of 3
    if (
      password.charCodeAt(i) + 1 === password.charCodeAt(i + 1) &&
      password.charCodeAt(i) + 2 === password.charCodeAt(i + 2)
    ) {
      increasingStraight = true
      break
    }
  }

  if (!increasingStraight) return false

  for (let i = 0; i <= passLength; i++) {
    // check for non-overlapping pairs
    if (
      !isNaN(password.charCodeAt(i + 1)) &&
      password.charCodeAt(i) !== password.charCodeAt(i + 1)
    ) {
      ++nonOverlap
      ++i
    }

    if (nonOverlap >= 2) {
      break
    }
  }

  return increasingStraight && nonOverlap >= 2
}

function validateForm({ username = '', password = '' }: Partial<State>) {
  return !!username.length && validatePassword(password!)
}

const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case 'updateUsername':
      return {
        ...state,
        username: action.payload,
        validForm: validateForm({
          username: action.payload as string,
          password: state.password,
        }),
      }

    case 'updatePassword':
      return {
        ...state,
        password: action.payload,
        validForm: validateForm({
          username: state.username,
          password: action.payload as string,
        }),
      }

    case 'reset':
      return initialState

    default:
      return state
  }
}

const ButtonWrap = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 16px 0;
`

export const LoginForm = () => {
  const [state, dispatch] = useReducer(reducer, initialState)
  const [loggedIn, setLoggedIn] = React.useState(
    localStorage.getItem('loggedIn') || 'false',
  )

  useEffect(() => {
    localStorage.setItem('loggedIn', loggedIn)
  }, [loggedIn])

  const login = useCallback(() => setLoggedIn('true'), [setLoggedIn])
  const logout = useCallback(() => setLoggedIn('false'), [setLoggedIn])

  return loggedIn === 'true' ? (
    <>
      <h2>Welcome back! You are logged in &#127863;</h2>
      <Button onClick={logout}>
        Logout
      </Button>
    </>
  ) : (
    <>
      <Popup>
        <h3>Login</h3>
        <Input
          label="Username"
          placeholder="foo-bar"
          value={state.username}
          gutter
          onChange={(value: string) =>
            dispatch({ type: 'updateUsername', payload: value })
          }
        />

        <Input
          label="Password"
          placeholder="******"
          value={state.password}
          onChange={(value: string) =>
            dispatch({ type: 'updatePassword', payload: value })
          }
        />

        <ButtonWrap>
          <Button onClick={() => dispatch({ type: 'reset' })} gutter>
            Clear
          </Button>
          <Button
            onClick={login}
            disabled={!state.validForm}
          >
            Login
          </Button>
        </ButtonWrap>

        {/* <p>{JSON.stringify(state)}</p> */}
      </Popup>
    </>
  )
}
