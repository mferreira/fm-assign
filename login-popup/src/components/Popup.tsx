import React from 'react'
import styled from 'styled-components'

const PopupBase = styled.div`
  width: 400px;
  background-color: #e6e6e6;
  border-radius: 4px;
  box-shadow: #b2b2b2 0px 0px 10px 0px;
  padding: 8px 14px;
  margin: auto;
  transform: translateY(50%);
  z-index: 500;
`

const Backdrop = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background-color: rgba(206, 206, 206, 0.51);
`

export const Popup: React.FC = ({ children }) => (
  <Backdrop>
    <PopupBase>
      { children }
    </PopupBase>
  </Backdrop>
)