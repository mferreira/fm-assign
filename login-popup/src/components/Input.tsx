import React from 'react'
import styled from 'styled-components'

interface Props extends Partial<HTMLInputElement> {
  onChange: (value: string) => void 
  gutter?: boolean
  label: string
}

interface WrapperProps {
  gutter?: Props['gutter']
}

const Wrapper = styled('div')<WrapperProps>`
  display: flex;
  ${p => (p.gutter ? 'margin-bottom: 10px;' : '')}
`

const Label = styled.label`
  margin-right: 8px;
`

const InputBase = styled.input`
  width: 100%;
`

export const Input: React.FC<Props> = ({
  label,
  value,
  placeholder,
  gutter,
  onChange,
}) => (
  <Wrapper gutter={gutter}>
    <Label htmlFor="user-name">{label}:</Label>
    <InputBase
      name="email"
      type="email"
      placeholder={placeholder}
      value={value}
      onChange={(e) => onChange(e.target.value)}
      required
    />
  </Wrapper>
)
