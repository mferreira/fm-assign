import * as React from 'react'
import { render } from 'react-dom'
import { LoginForm } from './components/LoginForm'

import './global.css'

function App() {
  return (
    <div>
      <LoginForm />
    </div>
  )
}

const rootElement = document.getElementById('root')
render(<App />, rootElement)
